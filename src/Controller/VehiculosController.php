<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Vehiculos Controller
 *
 * @property \App\Model\Table\VehiculosTable $Vehiculos
 * @method \App\Model\Entity\Vehiculo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VehiculosController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function lista()
    {
        $vehiculos = $this->Vehiculos->find('all', ['contain' => ['Users'], 'order' => ['Vehiculos.id']]);

        $this->set(compact('vehiculos'));
        $this->viewBuilder()
            ->setOption('serialize', ['vehiculos'])
            ->setOption('jsonOptions', JSON_FORCE_OBJECT);
    }

    /**
     * Ver method
     *
     * @param string|null $id Vehiculo id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function ver($id = null)
    {
        $vehiculo = $this->Vehiculos->get($id, ['contain' => ['Users']]);

        $this->set(compact('vehiculo'));
        $this->viewBuilder()
            ->setOption('serialize', ['vehiculo'])
            ->setOption('jsonOptions', JSON_FORCE_OBJECT);
    }

    /**
     * Agregar method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function agregar()
    {
        $success = false;
        $serialize = ['success'];
        $vehiculo = $this->Vehiculos->newEmptyEntity();
        
        if ($this->request->is('post')) {
            $user = $this->Authentication->getIdentity();
            $vehiculo = $this->Vehiculos->patchEntity($vehiculo, $this->request->getData());
            $vehiculo->creadoPor = $user->id;
            $vehiculo->fechaCreacion = date('Y-m-d H:i:s');
            
            if ($this->Vehiculos->save($vehiculo)) {
                $success = true;
                $this->set('vehiculo', $vehiculo);
                $serialize[] = 'vehiculo';
            }else{
                $error = 'The vehiculo could not be saved. Please, try again.';
                $this->set('error', $error);
                $serialize[] = 'error';
            }
        }
        $this->set('success', $success);
        $this->viewBuilder()
            ->setOption('serialize', $serialize)
            ->setOption('jsonOptions', JSON_FORCE_OBJECT);
    }

}
