<?php
declare(strict_types=1);

namespace App\Controller;
use Firebase\JWT\JWT;
use Cake\Utility\Security;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $this->Authentication->allowUnauthenticated(['login']);
    }

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function login()
    {
        $result = $this->Authentication->getResult();
        
        if ($result->isValid()) {
            $user = $result->getData();
            $payload = [
                'sub' => $user->id,
                'exp' => time() + 600,
            ];
            $auth = [
                'token' => JWT::encode($payload, Security::getSalt(), 'HS256'),
                'user' => $user->username
            ];
        } else {
            $this->response = $this->response->withStatus(401);
            $auth = ['error' => 'User or password is wrong'];
        }
        $this->set(compact('auth'));
        $this->viewBuilder()
            ->setOption('serialize', ['auth'])
            ->setOption('jsonOptions', JSON_FORCE_OBJECT);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->viewBuilder()
            ->setOption('serialize', ['user'])
            ->setOption('jsonOptions', JSON_FORCE_OBJECT);
    }

}
