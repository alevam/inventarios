<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vehiculo Entity
 *
 * @property int $id
 * @property int $tipo
 * @property string $marca
 * @property string $modelo
 * @property int $anio
 * @property float $potencia
 * @property int $existencias
 * @property int $creadoPor
 * @property \Cake\I18n\FrozenTime|null $fechaCreacion
 */
class Vehiculo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tipo' => true,
        'marca' => true,
        'modelo' => true,
        'anio' => true,
        'potencia' => true,
        'existencias' => true,
        'creadoPor' => true,
        'fechaCreacion' => true,
    ];

    protected $_virtual = ['tipo_desc', 'potencia_desc'];

    protected function _getTipoDesc()
    {
        return $this->tipo == 1 ? 'Sedan' : 'Motocicleta';
    }

    protected function _getPotenciaDesc()
    {
        return $this->potencia . ' ' . ($this->tipo == 1 ? 'HP' : 'CV');
    }
}
