<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * VehiculosFixture
 */
class VehiculosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'tipo' => 1,
                'marca' => 'Lorem ipsum dolor sit amet',
                'modelo' => 'Lorem ipsum dolor sit amet',
                'anio' => 1,
                'potencia' => 1,
                'existencias' => 1,
                'creadoPor' => 1,
                'fechaCreacion' => '2021-12-06 01:45:30',
            ],
        ];
        parent::init();
    }
}
